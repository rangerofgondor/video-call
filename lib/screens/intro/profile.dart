import 'package:flutter/material.dart';
import 'package:video_call/bloc/bloc.dart';
import 'package:video_call/constants/const.dart';

/// A Screen to create a Profile for a new user (Mainly name of the user)
/// Comes right after Intro Screen

class ProfileAdd extends StatefulWidget {
  @override
  _ProfileAddState createState() => _ProfileAddState();
}

class _ProfileAddState extends State<ProfileAdd> {
  String profile;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Spacer(),
            Hero(
              tag: 'icon',
              child: Image.asset(
                'assets/Logo Alternate.png',
                height: 60,
              ),
            ),
            Spacer(flex: 2),
            Padding(
              padding: const EdgeInsets.only(left: 32.0),
              child: Text(
                'Name?',
                style: bold,
              ),
            ),
            Spacer(flex: 2),
            Expanded(
              flex: 10,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: TextFormField(
                  autocorrect: true,
                  autofocus: true,
                  cursorColor: Colors.white,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(color: Colors.white54),
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                    hintText: "Enter a Name",
                    filled: true,
                    fillColor: Colors.white24,
                    hintStyle: inputThin,
                    errorStyle: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(color: Colors.red[300]),
                    prefixIcon: Icon(
                      Icons.account_circle,
                      color: Colors.white,
                    ),
                    contentPadding: EdgeInsets.all(24.0),
                    border: OutlineInputBorder(
                      borderRadius: themeBorder,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: themeBorder,
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                        borderRadius: themeBorder,
                        borderSide:
                            BorderSide(color: Colors.red[300], width: 2.0)),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: themeBorder,
                    ),
                    errorBorder: OutlineInputBorder(
                        borderRadius: themeBorder,
                        borderSide:
                            BorderSide(color: Colors.red[300], width: 2.0)),
                  ),
                  textInputAction: TextInputAction.done,
                  onEditingComplete: () => _submit(context),
                  validator: (data) {
                    if (data.isEmpty) {
                      return 'Please enter your name';
                    }
                    return null;
                  },
                  onSaved: (name) {
                    profile = name;
                  },
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: _doneButton(),
    );
  }

  Future<void> _submit(BuildContext context) async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      print("Profile Name Added: Name: $profile");
      BlocProvider.of<HomeBloc>(context)
          .add(HomeEventUpdateProfile(profile: profile));
      BlocProvider.of<ServerBloc>(context)
          .add(ServerEventUpdateHost(host: User(name: profile, ip: '')));
      Navigator.of(context).pushNamedAndRemoveUntil('/home', (route) => false);
    }
  }

  /// Custom Floating Action Button to display Get Started

  Widget _doneButton() {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.7,
      child: RaisedButton(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 15.0),
        child: Text('Done', style: buttonBold),
        shape: themeShape,
        onPressed: () {
          _submit(context);
        },
      ),
    );
  }
}
